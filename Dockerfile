FROM nodered/node-red-docker:latest
ADD flow.json /data/
ADD flow_cred.json /data/
ADD settings.js /data/